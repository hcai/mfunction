# MFunction

This library constructs Wigner D-Matrices and Mueller M-Matrices.  The former are described in many sources such as Sakurai, "Modern Quantum Mechanics", while the latter are defined in equation 9 of  arXiv:1702.03297 . 




## Dependencies

- Spacetime: https://gitlab.cern.ch/boudreau/spacetime.git


## Build instructions

```
git clone https://gitlab.cern.ch/boudreau/mfunction.git
cd mfunction
mkdir build
cd build
cmake ..
make
sudo make install
```
