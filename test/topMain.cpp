#include "MFunction/DMatrix.h"
#include "MFunction/UMatrixStash.h"
#include <iostream>
int main() {
  DMatrix D(2,0, M_PI/2.0, M_PI/2.0);
  std::cout << D << std::endl;
  return 0;
}
