// --------------------------------------------------------------------------//
//                                                                           //
//     class DMatrix                                                         //
//                                                                           //
//     This class computes a Wigner D-matrix in an efficient way.  It is     //
//     useful when one requires the full matrix. It uses matrix methods      //
//     avoiding Wigner's formula.                                            //
//                                                                           //
// --------------------------------------------------------------------------//
#ifndef __DMATRIX_H_
#define __DMATRIX_H_
#include "Eigen/Dense"
class DMatrix: public Eigen::MatrixXcd {


 public:
  
  // Construct a big D-matrix: note dim=2J+1 !!
  DMatrix(unsigned int dim, double alpha, double beta, double gamma);
  

  // The following are required when subclassing Eigen:
  template<typename OtherDerived>
    DMatrix(const Eigen::MatrixBase<OtherDerived>& other)
    : Eigen::MatrixXcd(other)
    { }


  template<typename OtherDerived>
    DMatrix& operator=(const Eigen::MatrixBase <OtherDerived>& other)
    {
     this->Eigen::MatrixXcd::operator=(other);
     return *this;
    }


};
#endif

