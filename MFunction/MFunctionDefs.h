//----------------------------------------------------------------------------//
//                                                                            //
//  Definitions for the MFunction library:                                    //
//                                                                            //
//  Joe Boudreau August 2019                                                  //
//                                                                            /
//----------------------------------------------------------------------------//
#ifndef __MFUNCTIONDEFS_H_
#define __MFUNCTIONDEFS_H_
#include <utility>
#include <memory>
class MMatrix;
class DMatrix;
namespace MFunction {

  typedef std::shared_ptr<DMatrix> DMatrixShrPtr;
  typedef std::shared_ptr<MMatrix> MMatrixShrPtr;


  
  // For convenience we define the "degree" of the MFunction to be
  // a pair of unsigned integers, J1 and J2.
  
  typedef std::pair<unsigned int, unsigned int> Degree;
  
  // -----------------------------------------------------------------
  //
  // This class induces an order onto MFunctionDegrees.  It is the
  // same order in which coefficients are listed in arXiv:1702.03297,
  // e. 11: This is a lexographical order in which the first index
  // J1 changes more rapidly than the second index J2.
  // 
  // -----------------------------------------------------------------
  
  class CanonicalSort{
  public:
    bool operator () (const Degree & a, const Degree & b) const {
      if (a.second < b.second) {
	return true;
      }
      else if (a.second==b.second) {
	if (a.first<b.first) {
	  return true;
	}
	else {
	  return false;
	}
      }
      else {
	return false;
      }
    }
  };

}
#endif
