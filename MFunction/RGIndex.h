//--------------------------------------------------------------------------//
//                                                                          //
// Classes:  RGIndex points into elements and generators of rotation group  //
//                                                                          //
//           Joe Boudreau August 2019                                       //
//                                                                          //
//--------------------------------------------------------------------------//
#ifndef __RGINDEX_H_
#define __RGINDEX_H_
class RGIndex{

 public:
  
  // Constructor:
 RGIndex(unsigned int j,int m):_j(j),_m(m),_index(j-m){}
  
  // Accessors:
  unsigned int j()     const {return _j;}
  int m()              const {return _m;}
  unsigned int dim()   const {return 2*_j+1;}
  
  // Cast operator:
  operator unsigned int () const {return _index;}
  
 private:
  
  unsigned int _j;
  int          _m;
  unsigned int _index;
};



#endif
