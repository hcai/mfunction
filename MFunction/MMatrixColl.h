//----------------------------------------------------------------------------//
//                                                                            //
// MMatrixColl: a collection of MMatrices, created and filled from the four   //
// angles describing the kinematics of top quark decay.                       //
//                                                                            //
// Joe Boudreau August 2019                                                   //
//                                                                            //
//----------------------------------------------------------------------------//
#ifndef _MMatrixColl_h_
#define _MMatrixColl_h_
#include "MFunction/MMatrixCollHeader.h"
#include "MFunction/MFunctionDefs.h"
#include "MFunction/MMatrix.h"
#include "MFunction/DMatrix.h"
#include "Eigen/Dense"
#include <memory>
#include <map>
#include <complex>

class MMatrixColl {
 public:

  // Constructor:
  MMatrixColl(double theta, double phi, double thetaStar, double phiStar);

  // Request a new matrix in the collection
  void create(unsigned int J1, unsigned int J2);

  // Access to the header:
  const MMatrixCollHeader & getHeader() const;

  class Cursor;
  Cursor cursor() const;

  // Obtain an element:
  std::complex<double> at(unsigned int J1, unsigned int J2, int m1, int m2) const;
  std::complex<double> operator () (const MMatrixColl::Cursor & c) const;

  // Obtain a vector of elements:
  Eigen::VectorXcd toVector() const;

  // The matrix of a transformation from complex coefficients to real + imaginary
  Eigen::MatrixXcd H() const;
  
  // For iteration:
  class Cursor {
  public:

    // Constructor:
    Cursor(const MMatrixCollHeader & header);

    // Move constructor:
    Cursor (Cursor &&);

    // Increment the iteration:
    void next();

    // Check you are at the end:
    operator bool() const;

    // Accessors:
    unsigned int j1() const;
    unsigned int j2() const;
    int m1() const;
    int m2() const ;

    // Flatten:
    unsigned int index() const;
    
  private:

    // Illegal operations:
    Cursor (const Cursor &)              = delete;
    Cursor & operator = (const Cursor &) = delete;

    // Private data:
    class Clockwork;
    std::unique_ptr<Clockwork> c;
  };

private:

  double                                                 theta;
  double                                                 phi;
  double                                                 thetaStar;
  double                                                 phiStar;
  mutable bool                                           isLocked;
  MMatrixCollHeader                                      header;
  std::map<MFunction::Degree, MFunction::MMatrixShrPtr>  mMatrix;
  std::map<unsigned int,      MFunction::DMatrixShrPtr>  dMatrix1;
  std::map<unsigned int,      MFunction::DMatrixShrPtr>  dMatrix2;
  
};

#include "MFunction/MMatrixColl.icc"

#endif
