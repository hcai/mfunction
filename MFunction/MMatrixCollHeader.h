// --------------------------------------------------------------------------//
//                                                                           //
//   class MMatrixCollHeader                                                 //
//                                                                           //
//   This class contains header information for a collection of MMatrices.   //
//   That information includes the J1 and J2 of each in the header.  It      //
//   is needed for many operations upon MMatrixColls.                        //
//                                                                           //
//   Joe Boudreau August 2019                                                //
//                                                                           //
// --------------------------------------------------------------------------//
#ifndef _MMatrixCollHeader_h_
#define _MMatrixCollHeader_h_
#include <set>
#include "MFunction/MFunctionDefs.h"
#include "Eigen/Dense"

class MMatrixCollHeader {
 public:
  

  // Declare an MMatrix to the headeer:
  void add(unsigned int J1, unsigned int J2) {
    degreeSet.insert(std::make_pair(J1,J2));
  }

  // Access to information stored in this "header: class is iterative.
  typedef std::set<MFunction::Degree,MFunction::CanonicalSort>::const_iterator ConstIterator;

  // Begin:
  ConstIterator begin() const { return degreeSet.begin();}

  // End:
  ConstIterator end()   const { return degreeSet.end();}

  // Length:
  unsigned int length() const;

  // The matrix of a transformation from complex coefficients to real + imaginary
  Eigen::MatrixXcd H() const;

  // Gets the flat-array index corresponding to J1, J2, M1, M2;
  unsigned int index(unsigned int J1, unsigned int J2, int M1, int M2) const;
  
private:
  
  std::set<MFunction::Degree,MFunction::CanonicalSort> degreeSet;
 
};

#include "MFunction/MMatrixCollHeader.icc"

#endif
