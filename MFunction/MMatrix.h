//----------------------------------------------------------------------------//
//                                                                            //
// class MMatrix                                                              //
//                                                                            //
// This class represents an "M-matrix" of four variables and having two       //
// integer labels, j₁ and j₂.                                                 //
//                                                                            //
// Joe Boudreau August 2019                                                   //
//                                                                            //
//----------------------------------------------------------------------------//
#ifndef _MMATRIX_H_   
#define _MMATRIX_H_
#include <complex>
#include <memory>
#include "MFunction/DMatrix.h"
#include "MFunction/MFunctionDefs.h"

class MMatrix {

public:
  
  // Constructor:
  MMatrix(unsigned int J1, unsigned int J2, double theta, double phi, double thetaStar, double phiStar);

  // Constructor:
  MMatrix(MFunction::DMatrixShrPtr d1, MFunction::DMatrixShrPtr d2);
  
  // Destructor:
  ~MMatrix();

  // Accessor:
  std::complex<double> operator () (int mPrime, int m) const;

  // Dimensions:
  unsigned int j1() const { return (d1->rows()-1)/2;}
  unsigned int j2() const { return (d2->rows()-1)/2;}
  
 private:

  MFunction::DMatrixShrPtr d1;
  MFunction::DMatrixShrPtr d2;
  double prefactor;
  
};
#endif
