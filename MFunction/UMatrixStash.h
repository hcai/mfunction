#ifndef _UMATRIXSTASH_H_
#define _UMATRIXSTASH_H_
#include "Eigen/Dense"
#include <map>
class UMatrixStash {

public:
  
  static Eigen::MatrixXcd & U(unsigned int dim);
  static Eigen::MatrixXcd & UDagger(unsigned int dim);
  
 private:

  static std::map<unsigned int, Eigen::MatrixXcd> stashU;
  static std::map<unsigned int, Eigen::MatrixXcd> stashUDagger;
  
};
#endif

