#include "MFunction/MMatrixColl.h"
#include <stdexcept>
#include <sstream>
#include <iostream> 
// Constructor:
MMatrixColl::MMatrixColl(double theta, double phi, double thetaStar, double phiStar):
  theta(theta),phi(phi), thetaStar(thetaStar), phiStar(phiStar),isLocked(false)
{
}

// Request a new matrix in the collection
void MMatrixColl::create(unsigned int J1, unsigned int J2) {
  // Check to see if the collection is locked, if so throw an exception:
  if (isLocked) throw std::runtime_error("Error in MMatrixColl:  Collection is locked due to prior access");
  
  // Check if it exists already; if so throw an exception.
  MFunction::Degree d(J1,J2);
  if (mMatrix.find(d)!=mMatrix.end()) {
    std::ostringstream msg;
    msg << "Error in MMatrix Coll:  Matrix of degree (" << d.first << "," << d.second << " already exists";
    throw std::runtime_error(msg.str());
  }
  
  // Check to see whether D1 exists, if not create it.
  MFunction::DMatrixShrPtr dptr1;
  auto d1=dMatrix1.find(J1);
  if (d1==dMatrix1.end()) {
    dptr1=MFunction::DMatrixShrPtr(new DMatrix(2*J1+1,phi, theta,0));
    dMatrix1[J1]=dptr1;
  }
  else {
    dptr1=d1->second;
  }
  // Check to see whether D2 exists, if not create it.
  MFunction::DMatrixShrPtr dptr2;
  auto d2=dMatrix2.find(J2);
  if (d2==dMatrix2.end()) {
    dptr2=MFunction::DMatrixShrPtr(new DMatrix(2*J2+1,phiStar, thetaStar,0));
    dMatrix2[J2]=dptr2;
  }
  else {
    dptr2=d2->second;
  }
  // Create the MFunction out of D1 and D2.
  MFunction::MMatrixShrPtr mPtr=MFunction::MMatrixShrPtr(new MMatrix(dptr1,dptr2));
  mMatrix[d]=mPtr;
  // Record creation in the header.
  header.add(J1,J2);
}


// Obtain an element:
std::complex<double> MMatrixColl::at(unsigned int J1, unsigned int J2, int m1, int m2) const {
  //
  // Collection is locked to new additions upon first access:
  //
  isLocked=true;
  //
  // Locate matrix of degree J1,J2 and return m1^th ,m2^th element or else just barf. 
  //
  MFunction::Degree d(J1,J2);
  auto mtx=mMatrix.find(d);
  if (mtx==mMatrix.end()) {
    std::ostringstream msg;
    msg << "Error in MMatrix Coll:  Matrix of degree (" << d.first << "," << d.second << " does not exist";
    throw std::runtime_error(msg.str());
    return 0.0;
  }
  else {
    return (mtx->second)->operator()(m1,m2);
  }
}



MMatrixColl::Cursor MMatrixColl::cursor() const {
  return Cursor(header);
}
  
Eigen::VectorXcd MMatrixColl::toVector() const {
  unsigned int length=header.length();
  Eigen::VectorXcd vec(length);
  Cursor cx=cursor();
  while (cx) {
    vec(cx.index())=at(cx.j1(),cx.j2(),cx.m1(),cx.m2());
    cx.next();
  }
  return std::move(vec);
}

Eigen::MatrixXcd MMatrixColl::H() const {
  return header.H();
}
