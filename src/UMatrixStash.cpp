#include "MFunction/UMatrixStash.h"
#include "Spacetime/Rotation.h"


std::map<unsigned int, Eigen::MatrixXcd> UMatrixStash::stashU;
std::map<unsigned int, Eigen::MatrixXcd> UMatrixStash::stashUDagger;

Eigen::MatrixXcd & UMatrixStash::U(unsigned int dim) {
  auto u=stashU.find(dim);
  if (u==stashU.end()) {
    static Rotation DY=Rotation   (M_PI/2.0*ThreeVector(0,1,0));
    static Rotation DZ=Rotation   (M_PI/2.0*ThreeVector(0,0,1));
    static Rotation DYInv=Rotation(-M_PI/2.0*ThreeVector(0,1,0));
    u=stashU.insert(stashU.begin(),std::make_pair(dim,(Eigen::MatrixXcd) DY.rep(dim)*DZ.rep(dim)*DYInv.rep(dim)));
  }
  return (*u).second;
}

Eigen::MatrixXcd & UMatrixStash::UDagger(unsigned int dim) {
  auto u=stashUDagger.find(dim);
  if (u==stashUDagger.end()) {
    Eigen::MatrixXcd X=U(dim);
    u=stashUDagger.insert(stashUDagger.begin(), std::make_pair(dim,X.adjoint()));
  }
  return (*u).second;
}
