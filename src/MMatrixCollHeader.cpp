#include "MFunction/MMatrixCollHeader.h"
#include "MFunction/MMatrixColl.h"
#include <stdexcept>
#include <sstream> 
#include <iostream>
Eigen::MatrixXcd MMatrixCollHeader::H() const {
  
  Eigen::MatrixXcd h=Eigen::MatrixXcd::Zero(length(),length());

  // Make a map:
  unsigned int counter(0);
  {
    MMatrixColl::Cursor cx(*this);
    while (cx) {
      if (cx.m1()==0 && cx.m2()==0) {   // m1=m2=0:  there is only a real part.
	h(counter++,cx.index())=1.0;
      }
      else { 
	if (cx.m1()>0 || (cx.m1()==0 && cx.m2()>0)) {  // Extract the real part:
	  unsigned int cconjIndex=index(cx.j1(),cx.j2(),-cx.m1(),-cx.m2());
	  h(counter,cx.index())= 0.5;
	  h(counter,cconjIndex)= 0.5*pow(-1.0,cx.m1());
	  counter++;
	}
	else { // Extract the imaginary part:
	}
      }
      cx.next();
    }
  }
  static const std::complex<double> I(0,1);
  {
    MMatrixColl::Cursor cx(*this);
    while (cx) {
      if (cx.m1()==0 && cx.m2()==0) {   // m1=m2=0:  there is only a real part.
      }
      else {
	if (cx.m1()>0 || (cx.m1()==0 && cx.m2()>0)) {  // Extract the real part:
	}
	else { // Extract the imaginary part:
	  unsigned int cconjIndex=index(cx.j1(),cx.j2(),-cx.m1(),-cx.m2());
	  h(counter,cx.index())=  -0.5*I;
	  h(counter,cconjIndex)=   0.5*I*pow(-1.0,cx.m1());
	  counter++;
	}
      }
      cx.next();
    }
  }
  

  
  return h;
  
}

// Gets the flat-array index corresponding to J1, J2, M1, M2;
unsigned int MMatrixCollHeader::index(unsigned int J1, unsigned int J2, int M1, int M2) const {
  MMatrixColl::Cursor cx(*this);
  while (cx) {
    if (cx.j1()==J1 && cx.j2()==J2 && cx.m1()==M1 && cx.m2()==M2) return cx.index();
    cx.next();
  }
  std::ostringstream stream;
  stream << "Error in MMatrixCollHeader:  illegal indices: j1="
	 << J1
	 << "j2="
	 << J2
	 << "m1="
	 << M1
	 << "m2="
	 << M2;
  throw std::runtime_error(stream.str());
  return 0;
};
