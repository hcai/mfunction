#include "MFunction/MMatrix.h"
#include "MFunction/RGIndex.h"
#include <iostream>

// Constructor:
MMatrix::MMatrix(unsigned int J1, unsigned int J2, double theta, double phi, double thetaStar, double phiStar):
  d1(MFunction::DMatrixShrPtr(new DMatrix(2*J1+1,phi,theta, 0))),
  d2(MFunction::DMatrixShrPtr(new DMatrix(2*J2+1,phiStar,thetaStar, 0))),
  prefactor(sqrt(d1->rows()*d2->rows()/4.0/M_PI))
{
}

// Constructor:
MMatrix::MMatrix(MFunction::DMatrixShrPtr d1, MFunction::DMatrixShrPtr d2):d1(d1),d2(d2),prefactor(sqrt(d1->rows()*d2->rows()/4.0/M_PI)) {
}

// Destructor:
MMatrix::~MMatrix(){
}

// Accessor:
  std::complex<double> MMatrix::operator () (int mPrime, int m) const {
    RGIndex i1Prime(j1(),mPrime);
    RGIndex i1(j1(),m);
    RGIndex i2Prime(j2(),m);
    RGIndex i2(j2(),0);
    std::complex<double> x1 = (*d1)(i1Prime,i1);
    std::complex<double> x2=  (*d2)(i2Prime,i2);
    return prefactor*x1*x2;
}


