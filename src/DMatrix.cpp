#include "MFunction/DMatrix.h"
#include "MFunction/UMatrixStash.h"
#include "MFunction/RGIndex.h"
#include <memory>
#include <complex>
#include <cmath>
#include <iostream>
typedef std::complex<double> Complex;


DMatrix::DMatrix(unsigned int dim, double alpha, double beta, double gamma):

  Eigen::MatrixXcd(Eigen::MatrixXcd::Identity(dim,dim)) {
  static const Complex I(0,1);


  if (alpha!=0) {
    Eigen::MatrixXcd D=Eigen::MatrixXcd::Zero(dim,dim);
    for (unsigned int i=0;i<dim;i++) {
      double m=(dim-1.0)/2.0-i;
      D(i,i)=std::exp(-I*(m*alpha));
    }
    (*this) *= D;
  }
  
  
  if (beta!=0) {
    Eigen::MatrixXcd D=Eigen::MatrixXcd::Zero(dim,dim);
    for (unsigned int i=0;i<dim;i++) {
      double m=(dim-1.0)/2.0-i;
      D(i,i)=std::exp(-I*(m*beta));
    }
    (*this) *= UMatrixStash::UDagger(dim);
    (*this) *= D;
    (*this) *= UMatrixStash::U(dim);
  }
  
  
  if (gamma!=0) {
    Eigen::MatrixXcd D=Eigen::MatrixXcd::Zero(dim,dim);
    for (unsigned int i=0;i<dim;i++) {
      double m=(dim-1.0)/2.0-i;
      D(i,i)=std::exp(-I*(m*gamma));
    }
    (*this) *= D;
  }
  
  
}
